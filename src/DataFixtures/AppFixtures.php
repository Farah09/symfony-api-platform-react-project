<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Demand;
use App\Entity\Customer;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * L'encodeur des mots de passe
     * 
     * @var UserPasswordEncoderInterface
     */
    
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) 
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');


        for($u = 0; $u < 10; $u++) {
            $user = new User();

            $chrono = 1;

            $hash = $this->encoder->encodePassword($user, "password");

            $user->setFirstName($faker->firstName())
                 ->setLastName($faker->lastname())
                 ->setEmail($faker->email())
                 ->setPassword($hash);

            $manager->persist($user);

            for($c = 0; $c < mt_rand(4, 20); $c++) {
                $customer = new Customer();
                $customer->setFirstName($faker->firstName())
                         ->setLastName($faker->lastName())
                         ->setService($faker->randomElement(['HR', 'PURCHASE', 'R&D', 'QSE']))
                         ->setEmail($faker->email())
                         ->setUser($user);
    
                $manager->persist($customer);
    
                for($i = 0; $i < mt_rand(3, 10); $i++) {
                    $demand = new Demand();
                    $demand->setSentAt($faker->dateTimeBetween('-6 months'))
                           ->settitle($faker->randomElement(['COMPTE', 'MATERIEL'])) 
                           ->setStatus($faker->randomElement(['IN PROGRESS', 'WAITING', 'COMPLETED']))
                           ->setCustomer($customer)
                           ->setChrono($chrono);
    
                    $chrono++;
    
                $manager->persist($demand);
                }
        }

      
        }

        $manager->flush();
    }
}
